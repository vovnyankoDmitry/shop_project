/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

var App = __webpack_require__(1);
var products = __webpack_require__(6);

console.log('Hi!!!!!');

new App(products, 'products-block');

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

var Cart = __webpack_require__(2);// в эту  переменную сохранится экспорт файла Cart
var ProductContent = __webpack_require__(5);



class App{
     
    constructor(products, container_id){
        this.cart = new Cart(products);// корзина

        this.products = products;
        this.container = document.getElementById(container_id);
        this.showProducts()// сработает сразу как только создастся обьект App
    }

    showProducts(){

        for(var j in this.products){

            var data = this.products[j];// выводим 

            var product = new ProductContent(data, this.cart.add);// создаем  обьект и передаем в него data

            this.container.appendChild(product.render());

        }
    }
}

module.exports = App;



/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

var ProductCart = __webpack_require__(3);

class Cart{
    constructor(products){

        this.productsCart = [];
        this.products = products;

        this.add = this.add.bind(this);
        this.remove = this.remove.bind(this);
        this.show =  this.show.bind(this);// bind закрепляет за данной фй this

        this.container = document.getElementById('cart');
        this.buttonBlock = document.getElementById('show-cart');

        // this.buttonBlock.onmouseenter = this.show;
        this.buttonBlock.onclick = this.show;

        this.fullsum = 0;
        this.fullQuantity = 0;
    }

    show(){
        this.container.classList.toggle("d-none");

    }

    add(e){//e - обьект события 
        var id = +e.target.dataset.productId //для data атрибутов в js

        var Product = this.isProductCart(id);

        if(Product){

            Product.quantity++;
            
            Product.sum += Product.price;
            

        }else{
            Product = this.getProduct(id);
            Product.sum = Product.price;
            Product.quantity = 1;
            this.productsCart.push(this.getProduct(id));
            }   
            this.fullQuantity ++;
            this.fullsum += Product.price;
            this.render();
    }

    remove(e){

        console.log(e);
        var id = +e.target.dataset.productId //для data атрибутов в js
        var Product = this.isProductCart(id);
        if(Product && Product.quantity > 1){

            Product.quantity--;
            Product.sum -= Product.price;

        }else{

            this.removeProduct(id);

            }  
            this.fullQuantity --;
            this.fullsum -= Product.price; 

        this.render();
        
    }

    isProductCart(id){
        for(var j in this.productsCart){

            if(+this.productsCart[j]['id'] === id){
                return this.productsCart[j];
            }

        }
        return false;
    }

    getProduct(id){
        for(var j in this.products){
            if(+this.products[j]['id'] === id){
                return this.products[j];
            }
        }
    }

    removeProduct(id){
        
    this.productsCart = this.productsCart.filter((product)=> {
    if(+product['id'] === id){
        return false;// для исключения продукта из массива
        }
    return true;
    });
}

    render(){
        this.container.innerHTML = '';
        for(var j in this.productsCart){
           var product = new ProductCart(this.productsCart[j], this.remove);

            this.container.appendChild(product.render())
        }

        this.buttonBlock.innerHTML = 'full sum :' +  this.fullsum + ' , full quantity:' + this.fullQuantity;
    }
}

module.exports = Cart;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {


var Product = __webpack_require__(4);

class ProductCart extends Product{

    constructor(dataProduct, callback) {
        super(dataProduct, callback);
    }

    render() {

        this.container.className = 'cart-product col-12 d-flex align-items-center mt-1 list-group-item-dark';       
        this.button.innerHTML = 'remove cart';
        this.name.className += ' col-sm-3';
        this.price.className += ' col-sm-3 mb-0';
        // this.imgWrap = document.createElement('div');
        // this.imgWrap.className = 'col-sm-3';
        // this.imgWrap.appendChild(this.image);

        this.quantity = document.createElement('div');
        
        this.quantity.className = 'col-sm-2';
        this.quantity.innerHTML = this.data.quantity;

        // this.container.appendChild(this.imgWrap);
        this.price.innerHTML = this.data.sum;
        this.container.appendChild(this.name);
        this.container.appendChild(this.quantity);
        this.container.appendChild(this.price);
        this.container.appendChild(this.button);

        return this.container;
    }

    
};
module.exports = ProductCart;

/***/ }),
/* 4 */
/***/ (function(module, exports) {


class Product{
    constructor(dataProduct, callback) {

        this.data = dataProduct;
        this.container = document.createElement('div');
        this.name = document.createElement('h4');
        this.image = document.createElement('img');
        this.price = document.createElement('p');
        this.button = document.createElement('button');

        this.name.className = 'p-name';
        this.image.className = 'p-img img-fluid';
        this.price.className = 'p-price';
        this.button.className = 'btn btn-outline-danger';
        this.button.type = 'button';

        this.name.innerHTML = this.data.name; //Взято из product.js
        this.image.src = this.data.image; //Взято из product.js
        this.price.innerHTML = this.data.price; //Взято из product.js  
        this.button.setAttribute('data-product-id', this.data.id);
        
        if(callback){
            this.button.onclick = callback;
        }
    }

}
module.exports = Product;

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

var Product = __webpack_require__(4);

class ProductContent extends Product{

    constructor(dataProduct, callback) {
        super(dataProduct, callback);
    }

    render() {

        this.container.className = 'content-product col-sm-4 mt-4 text-center';       
        this.button.innerHTML = 'to cart';

        this.container.appendChild(this.name);
        this.container.appendChild(this.image);
        this.container.appendChild(this.price);
        this.container.appendChild(this.button);

        return this.container;
    }

};

module.exports = ProductContent;

/***/ }),
/* 6 */
/***/ (function(module) {

module.exports = [{"id":"1","name":"Nokia 5","manufacturer":"China","price":5000,"image":"product.jpg","attributes":{"camera":"5Mp","diagonal":"5","os":"android"}},{"id":"2","name":"Nokia 6","manufacturer":"China","price":8000,"image":"product.jpg","attributes":{"camera":"5Mp","diagonal":"5,5","os":"android"}},{"id":"3","name":"Nokia 8","manufacturer":"China","price":12000,"image":"product.jpg","attributes":{"camera":"5Mp","diagonal":"6","os":"android"}},{"id":"4","name":"Acer Aspire","manufacturer":"China","price":15000,"image":"product.jpg","attributes":{"camera":"5Mp","diagonal":"15,6","os":"Windows"}},{"id":"5","name":"Acer Aspire 2","manufacturer":"China","price":25000,"image":"product.jpg","attributes":{"camera":"5Mp","diagonal":"15,6","os":"Windows"}},{"id":"6","name":"Acer Aspire 3","manufacturer":"China","price":40000,"image":"product.jpg","attributes":{"camera":"15Mp","diagonal":"17,2","os":"Windows"}},{"id":"7","name":"Samsung R57","manufacturer":"China","price":45000,"image":"product.jpg","attributes":{"camera":"15Mp","diagonal":"17,2","os":"Windows"}},{"id":"8","name":"Samsung R50","manufacturer":"China","price":18000,"image":"product.jpg","attributes":{"camera":"5Mp","diagonal":"15,6","os":"Windows"}},{"id":"9","name":"Samsung R52","manufacturer":"China","price":35000,"image":"product.jpg","attributes":{"camera":"5Mp","diagonal":"15,6","os":"Windows"}}];

/***/ })
/******/ ]);