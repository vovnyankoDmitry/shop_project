var HtmlWebpackPlugin = require('html-webpack-plugin');

var path = require('path');

module.exports = {
    devServer: {// настройка сервера
        contentBase: path.join(__dirname, 'public'),
        historyApiFallback: true,
        compress: true,
        port: 9000
    },

    entry: './index.js', // для вебпак
    mode: 'none',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname,'build')
    },
    
    plugins: [new HtmlWebpackPlugin({ // плагин автосохранения хтмл
        title: 'Learn shop',
        filename: 'public/index.html'
    })]
};