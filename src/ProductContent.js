var Product = require('./Product');

class ProductContent extends Product{

    constructor(dataProduct, callback) {
        super(dataProduct, callback);
    }

    render() {

        this.container.className = 'content-product col-sm-4 mt-4 text-center';       
        this.button.innerHTML = 'to cart';

        this.container.appendChild(this.name);
        this.container.appendChild(this.image);
        this.container.appendChild(this.price);
        this.container.appendChild(this.button);

        return this.container;
    }

};

module.exports = ProductContent;