var Cart = require('./Cart');// в эту  переменную сохранится экспорт файла Cart
var ProductContent = require('./ProductContent');



class App{
     
    constructor(products, container_id){
        this.cart = new Cart(products);// корзина

        this.products = products;
        this.container = document.getElementById(container_id);
        this.showProducts()// сработает сразу как только создастся обьект App
    }

    showProducts(){

        for(var j in this.products){

            var data = this.products[j];// выводим 

            var product = new ProductContent(data, this.cart.add);// создаем  обьект и передаем в него data

            this.container.appendChild(product.render());

        }
    }
}

module.exports = App;

